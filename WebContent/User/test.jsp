<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head >
    </head>

    <body>
        <h1>PayUForm </h1>
        <form action="<%=request.getContextPath() %>/myServlet"  name="payuform" method=POST >
            <input type="hidden" name="key"value="fB7m8s" />
            <input type="hidden" name="hash_string" value="" />
            <input type="hidden" name="hash" />

            <input type="hidden" name="txnid"/>

            <table>
                <tr>
                    <td><b>Mandatory Parameters</b></td>
                </tr>
                <tr>
                    <td>Amount: </td>
                    <td><input name="amount" value="${sessionScope.totalAmount}" /></td>
                    <td>First Name: </td>
                    <td><input name="firstname" id="firstname" value="${sessionScope.regVo.firstname }"  /></td>
                </tr>
                <tr>
                    <td>Email: </td>
                    <td><input name="email" id="email" value="${sessionScope.regVo.loginVo.email}" /></td>
                    <td>Phone: </td>
                    <td><input name="phone" value="8000708288" /></td>
                </tr>
                <tr>
                    <td>Product Info: </td>
                    <td colspan="3"><textarea name="productinfo" >${sessionScope.product}</textarea></td>
                </tr>
                <tr>
                    <td>Success URI: </td>
                    <td colspan="3"><input name="surl"  size="64" value="http://localhost:9090/scantocart/User/cartContents.jsp" /></td>
                </tr>
                <tr>
                    <td>Failure URI: </td>
                    <td colspan="3"><input name="furl"  size="64" value="http://localhost:9090/scantocart/User/cartContents.jsp" /></td>
                </tr>

                <tr>
                    <td colspan="3"><input type="hidden" name="service_provider" value="payu_paisa" /></td>
                </tr>
                <tr>
                    <td><b>Optional Parameters</b></td>
                </tr>
                <tr>
                    <td>Last Name: </td>
                    <td><input name="lastname" id="lastname" value="${sessionScope.regVo.lastname }"  /></td>
                    <td>Cancel URI: </td>
                    <td><input name="curl" value="http://localhost:9090/scantocart/User/cartContents.jsp" /></td>
                </tr>
                <tr>
                    <td>Address1: </td>
                    <td><input name="address1" /></td>
                    <td>Address2: </td>
                    <td><input name="address2"  /></td>
                </tr>
                <tr>
                    <td>City: </td>
                    <td><input name="city"  /></td>
                    <td>State: </td>
                    <td><input name="state"  /></td>
                </tr>
                <tr>
                    <td>Country: </td>
                    <td><input name="country"  /></td>
                    <td>Zipcode: </td>
                    <td><input name="zipcode"  /></td>
                </tr>
                <tr>
                    <td>UDF1: </td>
                    <td><input name="udf1"  /></td>
                    <td>UDF2: </td>
                    <td><input name="udf2"  /></td>
                </tr>
                <tr>
                    <td>UDF3: </td>
                    <td><input name="udf3"   /></td>
                    <td>UDF4: </td>
                    <td><input name="udf4"  /></td>
                </tr>
                <tr>
                    <td>UDF5: </td>
                    <td><input name="udf5"  /></td>
                    <td>PG: </td>
                    <td><input name="pg"  /></td>
                </tr>

                <td colspan="4"><input type="submit" value="Submit"  /></td>
                </tr>
            </table>
        </form>

    </body>
</html>



