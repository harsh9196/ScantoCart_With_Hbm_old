<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="h"%>

<div class="tygh-header clearfix">
	<div class="container-fluid  header-grid">
		<div class="row-fluid ">

			<div class="span16 header-bottom">
<!-- 				<div class="row-fluid ">
 -->

					<div class="span12 top-menu-grid">

						<span class="ty-menu__item ty-menu__menu-btn visible-phone">
							<a class="ty-menu__item-link"> <i class="ty-icon-short-list"></i>
								<span>Category</span>
						</a>
						</span>


						<div class="wrap-dropdown-multicolumns">
							<ul
								class="ty-menu__items cm-responsive-menu dropdown-multicolumns">


								<h:forEach items="${sessionScope.catList}" var="i">
									<li class="ty-menu__item fullwidth "><a
										class="ty-menu__item-toggle visible-phone cm-responsive-menu-toggle">
											<i class="ty-menu__icon-open ty-icon-down-open"></i> <i
											class="ty-menu__icon-hide ty-icon-up-open"></i>
									</a> <a
										href="<%=request.getContextPath()%>/viewProductController?flag=viewProduct&id=${i.categoryId}&cname=${i.categoryName}"
										class="ty-menu__item-link"> ${i.categoryName} </a> <span
										class="up-arrow"></span> <!-- 										<li class="ty-menu__item fullwidth ">
 --> <a
										class="ty-menu__item-toggle visible-phone cm-responsive-menu-toggle">
											<i class="ty-menu__icon-open ty-icon-down-open"></i> <i
											class="ty-menu__icon-hide ty-icon-up-open"></i>
									</a></li>
								</h:forEach>
							</ul>
						</div>

					</div>



				</div>
				<!-- <div class=" top-search">
									<div class="ty-search-block">
										<div class="search_toggle"></div>
										<form
											action="http://demos.templatemela.com/cscart/CST02/CST020042/"
											name="search_form" method="get" id="searchform">
											<input type="hidden" name="subcats" value="Y" /> <input
												type="hidden" name="pcode_from_q" value="Y" /> <input
												type="hidden" name="pshort" value="Y" /> <input
												type="hidden" name="pfull" value="Y" /> <input
												type="hidden" name="pname" value="Y" /> <input
												type="hidden" name="pkeywords" value="Y" /> <input
												type="hidden" name="search_performed" value="Y" /> <input
												type="text" name="q" value="" id="search_input"
												title="Search products"
												class="ty-search-block__input cm-hint" />
											<button title="Search" class="ty-search-magnifier"
												type="submit">
												<i class="ty-icon-search"></i>
											</button>
											<input type="hidden" name="dispatch" value="products.search" />

										</form>
									</div>
 -->
 <div class=" top-cart-content">
			<div class="ty-dropdown-box" id="cart_status_257">
				<div id="sw_dropdown_257"
					class="ty-dropdown-box__title cm-combination">
					<a href="${pageContext.request.contextPath}/CartdetailController?flag=search">View Cart &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp  </a>
					 <i class="ty-minicart__icon ty-icon-basket empty"></i> 
					<!-- <span class="ty-minicart-title empty-cart ty-hand">View
							Cart</span>  --> <i class="ty-icon-down-micro"></i>


					
				</div>				
				<!-- <div id="dropdown_257"
					class="cm-popup-box ty-dropdown-box__content hidden">

					<div
						class="cm-cart-content cm-cart-content-thumb cm-cart-content-delete">
						<div class="ty-cart-items">
							<div class="ty-cart-items__empty ty-center">Cart is empty</div>
						</div> -->

						<div
							class="cm-cart-buttons ty-cart-content__buttons buttons-container hidden">
							<div class="ty-float-left">
								<a href="${pageContext.request.contextPath}/CartdetailController?flag=search" rel="nofollow" class="ty-btn ty-btn__secondary">View
									cart</a>
							</div>
							<div class="ty-float-right">
								<a href="#" rel="nofollow" class="ty-btn ty-btn__primary">Checkout</a>
							</div>
						</div>

					</div>


				</div>
				<!--cart_status_257-->
			</div>



		</div>


	</div>
 
			</div>
		</div>


		<!--  <div class="span4 search-cart" >
 -->
		</div>

