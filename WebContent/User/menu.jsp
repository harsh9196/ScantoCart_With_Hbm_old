<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div class="span5 side-grid">
									
									<div class="ty-sidebox">
										<h2 class="ty-sidebox__title ">

											<span class="ty-sidebox__title-wrapper hidden-phone">View
												Cart</span> <span class="cm-combination" id="sw_sidebox_29">
												<span class="ty-sidebox__title-wrapper visible-phone">View
													Cart</span> <span class="ty-sidebox__title-toggle visible-phone">
													<i class="ty-sidebox__icon-open ty-icon-down-open"></i> <i
													class="ty-sidebox__icon-hide ty-icon-up-open"></i>
											</span>
											</span>


										</h2>
									</div>
									<div class="ty-sidebox">
										<h2 class="ty-sidebox__title ">

											<span class="ty-sidebox__title-wrapper hidden-phone">Category</span>
											<span class="cm-combination" id="sw_sidebox_51"> <span
												class="ty-sidebox__title-wrapper visible-phone">Category</span>
												<span class="ty-sidebox__title-toggle visible-phone">
													<i class="ty-sidebox__icon-open ty-icon-down-open"></i> <i
													class="ty-sidebox__icon-hide ty-icon-up-open"></i>
											</span>
											</span>


										</h2>
										<div class="ty-sidebox__body" id="sidebox_51">
											<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

											<div class="ty-menu ty-menu-vertical">
												<ul id="vmenu_51" class="ty-menu__items cm-responsive-menu">


													<c:forEach items="${sessionScope.catList}" var="i">

														<li
															class="ty-menu__item cm-menu-item-responsive dropdown-vertical__dir menu-level-">
															<div
																class="ty-menu__item-toggle visible-phone cm-responsive-menu-toggle">
																<i class="ty-menu__icon-open ty-icon-down-open"></i> <i
																	class="ty-menu__icon-hide ty-icon-up-open"></i>
															</div>
															<div class="ty-menu__item-arrow hidden-phone">
																<i class="ty-icon-right-open"></i><i
																	class="ty-icon-left-open"></i>
															</div>
															<div class="ty-menu__submenu-item-header">
																<a
																	href="<%=request.getContextPath()%>/viewProductController?flag=viewProduct&id=${i.categoryId}&cname=${i.categoryName}"
																	class="ty-menu__item-link">${i.categoryName}</a>
															</div> <!--        <div class="ty-menu__submenu">
        <ul class="ty-menu__submenu-items cm-responsive-menu-submenu">
        <li class="ty-menu__item cm-menu-item-responsive dropdown-vertical__dir menu-level-1">
        <div class="ty-menu__item-toggle visible-phone cm-responsive-menu-toggle">
        <i class="ty-menu__icon-open ty-icon-down-open"></i>
        <i class="ty-menu__icon-hide ty-icon-up-open"></i></div>
        <div class="ty-menu__item-arrow hidden-phone"></div>
        <i class="ty-icon-right-open"></i>
        
<li class="ty-menu__item cm-menu-item-responsive dropdown-vertical__dir menu-level-1">
<div class="ty-menu__item-toggle visible-phone cm-responsive-menu-toggle">
<i class="ty-menu__icon-open ty-icon-down-open"></i><i class="ty-menu__icon-hide ty-icon-up-open"></i></div>
<div class="ty-menu__item-arrow hidden-phone"><i class="ty-icon-right-open"></i><i class="ty-icon-left-open"></i></div>
<div class="ty-menu__submenu-item-header">
<a href="#"  class="ty-menu__item-link">Top Wear</a></div>
<div class="ty-menu__submenu">
</div>

<li class="ty-menu__item cm-menu-item-responsive dropdown-vertical__dir menu-level-1">
<div class="ty-menu__item-toggle visible-phone cm-responsive-menu-toggle">
<i class="ty-menu__icon-open ty-icon-down-open"></i><i class="ty-menu__icon-hide ty-icon-up-open"></i></div>
<div class="ty-menu__item-arrow hidden-phone"><i class="ty-icon-right-open"></i><i class="ty-icon-left-open"></i></div>
<div class="ty-menu__submenu-item-header">
<a href="#"  class="ty-menu__item-link">Bottom Wear</a></div>
<div class="ty-menu__submenu"></div>
</li>

</ul>

</div>
 -->

														</li>
													</c:forEach>





													<!-- <li class="ty-menu__item cm-menu-item-responsive dropdown-vertical__dir menu-level-">
<div class="ty-menu__item-toggle visible-phone cm-responsive-menu-toggle">
<i class="ty-menu__icon-open ty-icon-down-open"></i><i class="ty-menu__icon-hide ty-icon-up-open"></i></div>
<div class="ty-menu__item-arrow hidden-phone"><i class="ty-icon-right-open"></i><i class="ty-icon-left-open"></i></div>
<div class="ty-menu__submenu-item-header">
<a href="#"  class="ty-menu__item-link">Women's Wear</a>
</div>
<div class="ty-menu__submenu">
<ul class="ty-menu__submenu-items cm-responsive-menu-submenu">
<li class="ty-menu__item cm-menu-item-responsive dropdown-vertical__dir menu-level-1">
<div class="ty-menu__item-toggle visible-phone cm-responsive-menu-toggle"><i class="ty-menu__icon-open ty-icon-down-open"></i>
<i class="ty-menu__icon-hide ty-icon-up-open"></i></div><div class="ty-menu__item-arrow hidden-phone"><i class="ty-icon-right-open"></i>
<i class="ty-icon-left-open"></i></div><div class="ty-menu__submenu-item-header">
<a href="#"  class="ty-menu__item-link">Top Wear</a></div>
<div class="ty-menu__submenu"></div></li>

<li class="ty-menu__item cm-menu-item-responsive dropdown-vertical__dir menu-level-1">
<div class="ty-menu__item-toggle visible-phone cm-responsive-menu-toggle"><i class="ty-menu__icon-open ty-icon-down-open"></i>
<i class="ty-menu__icon-hide ty-icon-up-open"></i></div><div class="ty-menu__item-arrow hidden-phone">
<i class="ty-icon-right-open"></i><i class="ty-icon-left-open"></i></div>
<div class="ty-menu__submenu-item-header">
<a href="#"  class="ty-menu__item-link">Bottom Wear</a></div>
<div class="ty-menu__submenu"></div>
</li></ul>
</div>
</li>
 -->
													<!-- <li class="ty-menu__item cm-menu-item-responsive dropdown-vertical__dir ty-menu__item-active menu-level-">
<div class="ty-menu__item-toggle visible-phone cm-responsive-menu-toggle"><i class="ty-menu__icon-open ty-icon-down-open"></i>
<i class="ty-menu__icon-hide ty-icon-up-open"></i></div><div class="ty-menu__item-arrow hidden-phone"><i class="ty-icon-right-open"></i>
<i class="ty-icon-left-open"></i></div><div class="ty-menu__submenu-item-header">
<a href="#"  class="ty-menu__item-link">Kids Wear</a></div>
<div class="ty-menu__submenu">
</div>
</li>
 -->

												</ul>
												<!-- <li class="ty-menu__item cm-menu-item-responsive dropdown-vertical__dir menu-level-">
<div class="ty-menu__item-toggle visible-phone cm-responsive-menu-toggle">
<i class="ty-menu__icon-open ty-icon-down-open"></i><i class="ty-menu__icon-hide ty-icon-up-open"></i></div>
<div class="ty-menu__item-arrow hidden-phone"><i class="ty-icon-right-open"></i><i class="ty-icon-left-open"></i></div>
<div class="ty-menu__submenu-item-header">
<a href="#"  class="ty-menu__item-link">Accessories</a>
</div>
<div class="ty-menu__submenu">
<ul class="ty-menu__submenu-items cm-responsive-menu-submenu">
<li class="ty-menu__item cm-menu-item-responsive dropdown-vertical__dir menu-level-1">
<div class="ty-menu__item-toggle visible-phone cm-responsive-menu-toggle"><i class="ty-menu__icon-open ty-icon-down-open"></i>
<i class="ty-menu__icon-hide ty-icon-up-open"></i></div>
<div class="ty-menu__item-arrow hidden-phone">
<i class="ty-icon-right-open"></i>
<i class="ty-icon-left-open"></i></div><div class="ty-menu__submenu-item-header">
<a href="#"  class="ty-menu__item-link">Bags</a></div>
<div class="ty-menu__submenu">
</div></li>
<li class="ty-menu__item cm-menu-item-responsive dropdown-vertical__dir menu-level-1">
<div class="ty-menu__item-toggle visible-phone cm-responsive-menu-toggle"><i class="ty-menu__icon-open ty-icon-down-open"></i>
<i class="ty-menu__icon-hide ty-icon-up-open"></i></div><div class="ty-menu__item-arrow hidden-phone">
<i class="ty-icon-right-open"></i><i class="ty-icon-left-open"></i></div>
<div class="ty-menu__submenu-item-header">
<a href="#"  class="ty-menu__item-link">Watches</a></div>
<div class="ty-menu__submenu"></div>
</li>
<li class="ty-menu__item cm-menu-item-responsive dropdown-vertical__dir menu-level-1">
<div class="ty-menu__item-toggle visible-phone cm-responsive-menu-toggle"><i class="ty-menu__icon-open ty-icon-down-open"></i>
<i class="ty-menu__icon-hide ty-icon-up-open"></i></div><div class="ty-menu__item-arrow hidden-phone">
<i class="ty-icon-right-open"></i><i class="ty-icon-left-open"></i></div>
<div class="ty-menu__submenu-item-header">
<a href="#"  class="ty-menu__item-link">Shoes</a></div>
<div class="ty-menu__submenu"></div>
</li></ul>
</div>
</li> -->





											</div>
										</div>
									</div>
									<div class="ty-sidebox">
										<div class="ty-sidebox__body" id="sidebox_30">


											<ul class="ty-template-small">


												<li class="ty-template-small__item clearfix">
													<form action="#" method="post" name="product_form_3000011"
														enctype="multipart/form-data"
														class="cm-disable-empty-files  cm-ajax cm-ajax-full-render 
                cm-ajax-status-middle ">
														<input type="hidden" name="result_ids"
															value="cart_status*,wish_list*,checkout*,account_info*" />
														<input type="hidden" name="redirect_url"
															value="index.php?dispatch=categories.view&amp;category_id=225" />
														<input type="hidden" name="product_data[11][product_id]"
															value="11" />


													</form>

												</li>


											</ul>
										</div>
									</div>
								</div>
