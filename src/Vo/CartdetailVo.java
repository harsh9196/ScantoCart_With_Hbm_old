package Vo;

import java.io.Serializable;

public class CartdetailVo implements Serializable {
	
	private int cartDetailsId;
	private Cartmaster cartVo;
	private productVo productVO;
	private String amount;
	private String status;
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getCartDetailsId() {
		return cartDetailsId;
	}
	public void setCartDetailsId(int cartDetailsId) {
		this.cartDetailsId = cartDetailsId;
	}
	public productVo getProductVO() {
		return productVO;
	}
	public void setProductVO(productVo productVO) {
		this.productVO = productVO;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public Cartmaster getCartVo() {
		return cartVo;
	}
	public void setCartVo(Cartmaster cartVo) {
		this.cartVo = cartVo;
	}
	

}
