package Vo;

public class Cartmaster {
	
	private int cartId;
	private loginVo loginVo;
	private String cartStatus;
	
	
	public String getCartStatus() {
		return cartStatus;
	}
	public void setCartStatus(String cartStatus) {
		this.cartStatus = cartStatus;
	}
	public int getCartId() {
		return cartId;
	}
	public void setCartId(int cartId) {
		this.cartId = cartId;
	}
	public loginVo getLoginVo() {
		return loginVo;
	}
	public void setLoginVo(loginVo loginVo) {
		this.loginVo = loginVo;
	}
	
	
	


}
