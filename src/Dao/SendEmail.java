package Dao;

import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;

import utils.EmailTemplate;
import utils.commonutils;

public class SendEmail {
	
	public static void sendMail(String email, String subject, String content, String fromEmail,String fromPassword) {

		java.util.Properties properties = new java.util.Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		javax.mail.Session mailSession = javax.mail.Session.getInstance(properties);

		try {
			MimeMessage message = new MimeMessage(mailSession);

			message.setContent(content, "text/html");
			message.setSubject(subject);

			InternetAddress sender = new InternetAddress(fromEmail, "TaleSpin");
			InternetAddress receiver = new InternetAddress(email);
			message.setFrom(sender);
			message.setRecipient(Message.RecipientType.TO, receiver);
			message.saveChanges();

			javax.mail.Transport transport = mailSession.getTransport("smtp");
			transport.connect("smtp.gmail.com", 587, fromEmail, fromPassword);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void SendMailResponse(HttpServletRequest request,String email,String subject,String fromEmail,String fromPassword){
		  
		EmailTemplate emailtemplate=new EmailTemplate();
		String Email=(String) request.getSession().getAttribute("EmailID");
		String portalUrl=commonutils.getPortalURL(request);
		String resetLink=portalUrl+"/User/resetpassword.jsp?emailid=" + Email;
		System.out.println(resetLink);
		// Recipient's email ID needs to be mentioned.
	      String to = email;

	      // Sender's email ID needs to be mentioned
	      final String from = fromEmail;
	      final String username = "XYZ";//change accordingly
	      final String password = fromPassword;//change accordingly

	      // Assuming you are sending email through relay.jangosmtp.net
	      String host = "smtp.gmail.com";

	      Properties props = new Properties();
	      props.put("mail.smtp.auth", "true");
	      props.put("mail.smtp.starttls.enable", "true");
	      props.put("mail.smtp.host", host);
	      props.put("mail.smtp.port", "587");

	      Session session = Session.getInstance(props,
	         new javax.mail.Authenticator() {
	            protected PasswordAuthentication getPasswordAuthentication() {
	               return new PasswordAuthentication(from, password);
	            }
	         });

	      try {

	         // Create a default MimeMessage object.
	         Message message = new MimeMessage(session);

	         // Set From: header field of the header.
	         message.setFrom(new InternetAddress(from));

	         // Set To: header field of the header.
	         message.setRecipients(Message.RecipientType.TO,
	            InternetAddress.parse(to));

	         // Set Subject: header field
	         message.setSubject(subject);

	         // This mail has 2 part, the BODY and the embedded image
	         MimeMultipart multipart = new MimeMultipart("related");

	         //  (the html)
	         BodyPart messageBodyPart = new MimeBodyPart();
	         System.out.println("After BodyPArt");
	         String htmlText =emailtemplate.ResetPasswordTemplate(resetLink, portalUrl);
	         messageBodyPart.setContent(htmlText, "text/html");
	         // add it
	         multipart.addBodyPart(messageBodyPart);
             System.out.println("Final multipart");

            
	       message.setContent(multipart);
	         // Send message
	 
	       
	         Transport.send(message);
	         System.out.println(message);

	         System.out.println("Sent mail successfully....");

	      } catch (Exception e) {
	    	  e.printStackTrace();
	      }
	   }
	
		
}
