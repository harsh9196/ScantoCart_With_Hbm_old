package Dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import Vo.CartdetailVo;
import Vo.loginVo;
import Vo.regVo;

public class CartdetailDao {

	public List searchCartdetail() {

		List ls = new ArrayList();

		try {
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

			Session session = sessionFactory.openSession();

			Transaction tr = session.beginTransaction();

			Query q = session.createQuery("from CartdetailVo where status='pending'");
			ls = q.list();

			for (Iterator iterator = ls.iterator(); iterator.hasNext();) {
				CartdetailVo object = (CartdetailVo) iterator.next();

				System.out.println(object.getProductVO().getProductName());
			}

			tr.commit();

			session.close();
		} catch (Exception z) {
			z.printStackTrace();
		}
		return ls;

	}

	public List searchUserById(loginVo vo) {

		List ls = new ArrayList();
		try {
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

			Session session = sessionFactory.openSession();

			Transaction tr = session.beginTransaction();
			// System.out.println(vo.getLoginId());
			Query q = session.createQuery("from regVo where loginId='" + vo.getLoginId() + "'");
			ls = q.list();
			// System.out.println("List"+ls);
			for (Iterator iterator = ls.iterator(); iterator.hasNext();) {
				regVo object2 = (regVo) iterator.next();

				System.out.println("Firstname" + object2.getFirstname());
				System.out.println("Email" + object2.getLoginVo().getEmail());
			}

			tr.commit();

			session.close();
		} catch (Exception z) {
			z.printStackTrace();
		}
		return ls;

	}

	public List<CartdetailVo> searchCartdetailbyLogin(loginVo loginVo) {
		List ls = new ArrayList();
		try {
			SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
			Session session = sessionFactory.openSession();
			Query q = session.createQuery("from CartdetailVo where cartVo.loginVo.loginId='"+loginVo.getLoginId()+"' and status='pending'");
			ls = q.list();
			session.close();
		} catch (Exception z) {
			z.printStackTrace();
		}
		return ls;
	}

}
