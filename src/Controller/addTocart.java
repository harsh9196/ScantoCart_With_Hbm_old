package Controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Dao.CartdetailDao;
import Vo.CartdetailVo;
import Vo.loginVo;
import Vo.regVo;

/**
 * Servlet implementation class addTocart
 */
@WebServlet("/addTocart")
public class addTocart extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public addTocart() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
CartdetailVo cartdetailVo =new CartdetailVo();
		
		CartdetailDao cartdetailDao=new CartdetailDao();
		List<CartdetailVo> ls=cartdetailDao.searchCartdetail();
		
		String product="";
		int totalAmount = 0;
		for (CartdetailVo cartdetailVo2 : ls) {
			if(product.isEmpty()){
				product = cartdetailVo2.getProductVO().getProductName();
			}else if(!product.isEmpty()){
				product=product+","+ cartdetailVo2.getProductVO().getProductName();
			}
			totalAmount = totalAmount + Integer.parseInt(cartdetailVo2.getProductVO().getProductPrice());
			
		}
		loginVo vo = new loginVo();
		vo.setLoginId((int)request.getSession().getAttribute("userID"));
		
		HttpSession session = request.getSession();
		List<regVo> ls1=cartdetailDao.searchUserById(vo);
		System.out.println(ls1.get(0).getLoginVo().getEmail());
		session.setAttribute("regVo",ls1.get(0));
		session.setAttribute("product",product);
		session.setAttribute("totalAmount",totalAmount);
		session.setAttribute("List", ls);
		
		response.sendRedirect("User/cartContents.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
