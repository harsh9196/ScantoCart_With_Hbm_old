package Controller;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Dao.CartdetailDao;
import Dao.productDao;
import Vo.CartdetailVo;
import Vo.loginVo;
import Vo.productVo;
import Vo.regVo;

/**
 * Servlet implementation class CartdetailController
 */
@WebServlet("/CartdetailController")
public class CartdetailController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CartdetailController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String flag = request.getParameter("flag");
		if (flag.equals("search")) {
			search(request, response);
			response.sendRedirect("User/cartContents.jsp");
		} else if (flag.equals("payment")) {
			search(request, response);
			response.sendRedirect("User/payuform.jsp");
		} else if (flag.equals("surl")) {
			success(request, response);
			search(request, response);
			response.sendRedirect("User/cartContents.jsp");
		} else if (flag.equals("furl") || flag.equals("curl")) {
			search(request, response);

		} else if (flag.equals("admin")) {
			response.sendRedirect("Admin/Index.jsp");
		} else if (flag.equals("user")) {
			response.sendRedirect("User/Index.jsp");
		}
	}

	private void success(HttpServletRequest request, HttpServletResponse response) {
		List<CartdetailVo> cartdetailVoList = (List<CartdetailVo>) request.getSession().getAttribute("List");
		productDao dao = new productDao();

		Iterator<CartdetailVo> iterator = cartdetailVoList.iterator();
		while (iterator.hasNext()) {
			CartdetailVo cartdetailVo = (CartdetailVo) iterator.next();
			cartdetailVo.setStatus("done");
			dao.insertProductList(cartdetailVo);
		}
		// dao.insertProductList(cartdetailVoList);
	}

	private void search(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		CartdetailVo cartdetailVo = new CartdetailVo();

		loginVo loginVo = new loginVo();
		loginVo.setLoginId((Integer) request.getSession().getAttribute("userID"));

		CartdetailDao cartdetailDao = new CartdetailDao();
		List<CartdetailVo> ls = cartdetailDao.searchCartdetailbyLogin(loginVo);
		
			
		System.out.println("searchCartdetailbyLogin--->>>" + ls.size());

		request.getSession().setAttribute("List", ls);

		String product = "";
		int totalAmount = 0;
		for (Iterator iterator = ls.iterator(); iterator.hasNext();) {
			CartdetailVo cartdetailVo2 = (CartdetailVo) iterator.next();
			if (product.isEmpty()) {
				product = cartdetailVo2.getProductVO().getProductName();
			} else if (!product.isEmpty()) {
				product = product + "," + cartdetailVo2.getProductVO().getProductName();
				System.out.println(product);
			}
			if (cartdetailVo2.getProductVO().getOffers().contains("N/A")) {
				totalAmount = totalAmount + Integer.parseInt(cartdetailVo2.getProductVO().getProductPrice());
			}
			else {
				int productP = Integer.parseInt(cartdetailVo2.getProductVO().getProductPrice());
				int offerP = Integer.parseInt(cartdetailVo2.getProductVO().getOffers());
				int finalA = productP - (productP*offerP)/100;
				System.err.println("finalA----->>"+finalA);
				productVo productVo=cartdetailVo2.getProductVO();
				productVo.setProductPrice(String.valueOf(finalA));
				cartdetailVo2.setProductVO(productVo);
				totalAmount = totalAmount + finalA;
			}
		}
		System.out.println("after foreach");

		loginVo vo = new loginVo();
		vo.setLoginId((int) request.getSession().getAttribute("userID"));

		HttpSession session = request.getSession();
		List<regVo> ls1 = cartdetailDao.searchUserById(vo);
		// System.out.println(ls1);

		System.err.println(ls1.get(0).getLoginVo().getEmail());
		System.err.println(ls1.get(0).getFirstname());
		System.err.println(ls1.get(0).getLastname());

		session.setAttribute("reg", ls1);
		session.setAttribute("product", product);
		session.setAttribute("totalAmount", totalAmount);
		session.setAttribute("List", ls);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
