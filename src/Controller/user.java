package Controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Dao.viewProductDao;
import Vo.FileVO;
import Vo.categoryVo;
import Vo.productVo;

/**
 * Servlet implementation class user
 */
@WebServlet("/user")
public class user extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public user() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		/*response.sendRedirect(request.getContextPath()+"/User/index.jsp");*/
		//System.out.println("hie");
		//response.sendRedirect(request.getContextPath()+"/viewProductController?flag=viewProduct&id=1&cname=Men's Wear");
		System.out.println("hiiiiii");
		int id=Integer.parseInt("1");
		String cname = "Men's Wear";
		
		categoryVo categoryvo =new categoryVo();
				categoryvo.setCategoryId(id);
		
				productVo vo = new productVo();
				vo.setCategoryvo(categoryvo);
		
		
				viewProductDao viewProductDAO=new viewProductDao();
				List ls1=viewProductDAO.searchProductbyCategory(vo);
				
				
				
		FileVO filevo =new FileVO();
		
		List ls=viewProductDAO.searchProduct(filevo);
		HttpSession session = request.getSession();
		
		session.setAttribute("List", ls);		
		session.setAttribute("pList", ls1);	
		session.setAttribute("cname", cname);
		
		response.sendRedirect(request.getContextPath()+"/User/Productdetails.jsp");
	}

}
