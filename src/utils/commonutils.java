package utils;

import javax.servlet.http.HttpServletRequest;

public class commonutils {
	public static String getPortalURL(HttpServletRequest request) {
		return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
				+ request.getContextPath();
	}

}
