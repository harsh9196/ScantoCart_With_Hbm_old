package utils;

public class EmailTemplate {
	private StringBuffer templateMaker;
	
	public String ResetPasswordTemplate(String link, String extraLink) {
		System.out.println("In resetPasswordTemplate");
		templateMaker = new StringBuffer();
		templateMaker.append("<!DOCTYPE html> <html> <head>");
		templateMaker.append("<title>Forgot Password</title>");
		templateMaker.append(
				"<link href=\"https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic\" rel=\"stylesheet\" type=\"text/css\">");
		templateMaker.append(
				"<link href=\"https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700\" rel=\"stylesheet\">");
		templateMaker.append("<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">");
		templateMaker.append("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
		templateMaker.append("</head>");
		templateMaker.append("<body style=\"background:#ebecec;margin: 0;\">");

		templateMaker.append("<div style=\"padding: 35px 60px 25px;\">");
		templateMaker.append("<div style=\"margin-bottom:35px;\">");
		templateMaker.append("<img src=\"cid:image0\" alt=\"\">");
		templateMaker.append("</div>");
		templateMaker.append("<div style=\"background:#FEFEFE;font-family: roboto;font-size: 14px;\">");
		templateMaker.append(
				"<div style=\"background: #ba1324; text-align: center; padding: 35px 0px;\"><img style=\"vertical-align: middle;\" src=\"cid:image5\" alt=\"\">");
		templateMaker.append("</div>");
		templateMaker.append("<div style=\"padding: 30px 20px 50px;\">");
		templateMaker.append(
				"<div style=\"font-size: 18px; text-align: center; font-weight: bold; font-family:Roboto Condensed; color: #5b5b5b;margin-bottom: 25px\">Forgot Your password? No worries....");
		templateMaker.append("</div>");
		templateMaker.append(
				"<p style=\"color: #5B5B5B; font-weight: 300; margin-bottom: 20px; margin-top: 0px;\">Look like you have forgotten your password. No problem! Click the button below to reset your password.</p>");
		templateMaker.append("<div style=\"text-align: center;\">");
		templateMaker
				.append("<a style=\"margin-bottom: 30px; background:#ba1324; border: medium none; border-radius: 30px; color: rgb(254, 254, 254); font-size: 14px; font-family: roboto; padding: 5px 40px;text-decoration: none;\" href=\""
						+ link + "\">Reset</a><br/>");
		templateMaker.append("</div>");
		templateMaker.append("<div>");
		templateMaker.append(
				"<span style=\"color:#5b5b5b; display: block; margin-bottom: 15px; font-weight: 300;\">Thank you!</span>");
		templateMaker.append(
				"<br/><span style=\"color:#5b5b5b; display: block; margin-bottom: 15px; font-weight: 300;\">ScanToCart team</span>");
		templateMaker.append("</div></div></div>");
		templateMaker
				.append("<div style=\"text-align: right; margin-top: 15px; word-spacing: 5px;margin-bottom:10px;\">");
		templateMaker.append("<a href=\"javascript:;\"><img src=\"cid:image1\" alt=\"\"></a>&nbsp;");
		templateMaker.append("<a href=\"javascript:;\"><img src=\"cid:image2\" alt=\"\"></a>&nbsp;");
		templateMaker.append("<a href=\"javascript:;\"><img src=\"cid:image3\" alt=\"\"></a>&nbsp;");
		templateMaker.append("<a href=\"javascript:;\"><img src=\"cid:image4\" alt=\"\"></a>&nbsp;");
		templateMaker.append("</div>");
		templateMaker.append(
				"<div style=\"color: #5b5b5b; font-family: roboto; text-align: center; font-size: 14px; font-weight: 300;\">");
		templateMaker.append(
				"<span style=\"display: block;margin-bottom:5px;\">Please do not reply to this Email. Reply to this Email will not be responded to or read</span>");
		templateMaker.append(
				"&nbsp;<br/><span>scantocart &copy; 2017  All rights reserved.</span>");
		templateMaker.append("</div></div>");
		templateMaker.append("</body>");
		templateMaker.append("</html>");
		return templateMaker.toString();
	}

}
